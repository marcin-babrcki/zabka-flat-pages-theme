<?php

if ( ! defined( 'ZFPT_VERSION' ) ) {
	
	define( 'ZFPT_VERSION', '2.5.0' );
}

add_filter( 'get_site_icon_url', '__return_false' );

function wpdocs_remove_menus(){
   
if(!current_user_can('administrator')) {
  remove_menu_page( 'index.php' );                  //Dashboard
  remove_menu_page( 'edit.php' );                   //Posts
  //remove_menu_page( 'upload.php' );                 //Media
  //remove_menu_page( 'edit.php?post_type=page' );    //Pages
  remove_menu_page( 'edit-comments.php' );          //Comments
  remove_menu_page( 'themes.php' );                 //Appearance
  remove_menu_page( 'plugins.php' );                //Plugins
  remove_menu_page( 'users.php' );                  //Users
  remove_menu_page( 'tools.php' );                  //Tools
  remove_menu_page( 'options-general.php' );        //Settings  
  remove_menu_page( 'admin.php?page=jj4t3-logs' );        //Plugin Custom Css Js 	 

}
}
add_action( 'admin_menu', 'wpdocs_remove_menus' );



function zabkaflatpagestheme_scripts() {
	wp_enqueue_style( 'zabkaflatpagestheme-style', get_stylesheet_uri(), array(), ZFPT_VERSION );
	wp_enqueue_script( 'zabkaflatpagestheme-main-js', get_template_directory_uri() . '/js/main.js', array(), ZFPT_VERSION, true );

}
add_action( 'wp_enqueue_scripts', 'zabkaflatpagestheme_scripts' );

remove_action('template_redirect', 'redirect_canonical');


