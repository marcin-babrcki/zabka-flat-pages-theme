<?php

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="color-scheme" content="dark light">
	<meta id="Viewport" name="viewport" content="initial-scale=1.0, maximum-scale=2, minimum-scale=1.0, user-scalable=yes, width=device-width">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

