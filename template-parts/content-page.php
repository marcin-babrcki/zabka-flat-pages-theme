
<header>
<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
</header>

<?php
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post(); 
		the_content();	
	} 
}
?>

